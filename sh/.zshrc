## (here .zprofile file is sourcing ~/.profile)

## TODO: git status

## bourne-compatible env
test -d $HOME/.sh && source $HOME/.sh/env

## work specific env
test -f $HOME/.workenv && source $HOME/.workenv

## zsh
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=100000
zstyle :compinstall filename '~/.zshrc'
zstyle ':completion:*' rehash true	# persistant rehash
autoload -Uz compinit; compinit
autoload -Uz colors; colors
setopt autocd
setopt HIST_IGNORE_DUPS     # ignore duplicate lines in history
unsetopt beep notify

## prompt setup, I want: "$? (vi-mode) $HOST:/$PWD"
function zle-line-init zle-keymap-select() {
typeset _normal_string="(Normal)"
typeset _insert_string="(Insert)"
typeset _ret="%(?..%{$fg[red]%}%? %{$reset_color%})"
typeset _mode="${${KEYMAP/vicmd/$_normal_string}/(main|viins)/$_insert_string} "
PROMPT="${_ret}${_mode}%m:%~ » "
zle reset-prompt
}

zle -N zle-line-init
zle -N zle-keymap-select

## bindings
export KEYTIMEOUT=1
bindkey -v
source $SHDIR/emacs_schim.zsh
bindkey '^P' up-history
bindkey '^N' down-history
bindkey '^W' backward-kill-word
bindkey '^[[1~' beginning-of-line
bindkey '^[[4~' end-of-line
bindkey -M viins '\e.' insert-last-word

alias r="source ~/.zshrc"
