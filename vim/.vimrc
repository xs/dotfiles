" xasa/xs's vimrc
"

" plugin manager using https://github.com/junegunn/vim-plug
call plug#begin('~/.vim/plugged')

Plug 'xsgb/vim-monochrome'
Plug 'tpope/vim-sensible'
Plug 'kien/ctrlp.vim'
Plug 'chase/vim-ansible-yaml'
Plug 'christoomey/vim-tmux-navigator'
Plug 'tpope/vim-fugitive'
Plug 'scrooloose/syntastic'
Plug 'rking/ag.vim'
Plug 'godlygeek/tabular'
Plug 'plasticboy/vim-markdown'
Plug 'xuhdev/vim-latex-live-preview'
Plug 'majutsushi/tagbar'
Plug 'luisjure/csound'

call plug#end()

" appearance
set ttyfast
set ruler
highlight NonText ctermfg=59 ctermbg=0 cterm=NONE guifg=#414e58 guibg=#232c31 gui=NONE
highlight ColorColumn ctermbg=blue
syntax match Tab /\t/
hi Tab gui=underline
call matchadd('ColorColumn', '\%81v', 100)

set listchars=tab:……,trail:.
set list

set statusline=
set statusline +=%4*\ %<%F%*            "full path
set statusline +=%2*%m%*                "modified flag
set statusline +=%3*%=%5l%*             "current line
set statusline +=%4*/%L%*               "total lines
set statusline +=%3*%4v\ %*             "virtual column number
set statusline +=%4*0x%04B\ %*          "character under cursor
set t_co=256
color monochrome
syntax on
set nonumber " no numbers by default, toogle with <leader>n

" behaviours
let mapleader=','
let localelader=','
nmap <leader>n :set nu!<cr>
nmap <leader>N :set rnu!<cr>
nmap <leader>l :set list!<cr>
nmap <leader>s :if exists("g:syntax_on") <bar>
    \   syntax off<bar>
    \ else<bar>
    \   syntax on<bar>
    \ endif<cr>
nmap <leader>p :set paste!<cr>
nmap <leader>h :set nohlsearch!<cr>
nmap <leader>s :<esc>:Ag 
set nojoinspaces

" open splits in a sane way
set splitright
set splitbelow
autocmd FileType help wincmd L

" file indentation
" set showmatch
set smarttab
set autoindent
set smartindent
set cinoptions={1s,f1s
filetype indent on
filetype plugin on

" tabulation control
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab

" line break
set lbr
set sbr=>

" auto commands
autocmd! bufwritepost ~/.vimrc source %
autocmd! bufwritepost ~/.Xresources !(xrdb -merge ~/.Xresources)
autocmd BufRead,BufNewFile *.{yml,yaml} setlocal ft=ansible

" undo, swap and backup dirs
set undodir=~/.vim/undo/
set backupdir=~/.vim/backup/
set directory=~/.vim/tmp/
set undofile
set undolevels=1000

" LaTeX live preview
let g:livepreview_previewer = 'zathura'
set updatetime=1000
nmap <F2> :LLPStartPreview<cr>

" toogle tagbars
nmap <leader>t :TagbarToggle<cr>

" tagbar definitions
"" ansible
let g:tagbar_type_ansible = {
    \ 'ctagstype' : 'ansible',
    \ 'kinds' : [
        \ 't:tasks'
    \ ],
    \ 'sort' : 0
\ }

"" markdown
let g:tagbar_type_markdown = {
    \ 'ctagstype' : 'markdown',
    \ 'kinds' : [
        \ 'h:Heading_L1',
        \ 'i:Heading_L2',
        \ 'k:Heading_L3'
    \ ]
\ }
